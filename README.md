# Backend DOTNET API Documentation

This backend API is built using .NET and serves as the core component for handling mocked users list.

## Table of Contents

- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Endpoints](#endpoints)
- [License](#license)

## Prerequisites

Before running this API locally, ensure you have the following installed:

- [.NET SDK](https://dotnet.microsoft.com/download)
- [Your preferred code editor](#)

## Installation

1. Clone this repository to your local machine.
   ```bash
   git clone https://gitlab.com/dacost.dev/backend-dotnet.git
   ```

2. Get into the project directory
   ```bash
   cd net-back
   ```

3. Install dependencies
   ```bash
   dotnet restore
   ```

4. Run project
   ```bash
   dotnet run
   ```

## Endpoints

### Example Endpoints

- `GET /api/users`: Get a list of mocked users.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.