using Microsoft.AspNetCore.Mvc;

namespace YourNamespace.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UserController : ControllerBase
    {
        [HttpGet]
        public ActionResult<IEnumerable<object>> GetAllUsers()
        {
            var users = new List<object>
            {
                new { Id = Guid.NewGuid(), Name = "User1", Email = "user@example.com" },
                new { Id = Guid.NewGuid(), Name = "User2", Email = "user@example.com" },
                new { Id = Guid.NewGuid(), Name = "User3", Email = "user@example.com" },
                new { Id = Guid.NewGuid(), Name = "User4", Email = "user@example.com" },
                new { Id = Guid.NewGuid(), Name = "User5", Email = "user@example.com" },
            };

            return Ok(users);
        }
    }
}